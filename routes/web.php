<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TempeeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/cell',[TempeeController::class, 'index'])->name('cell');

Route::get('/tambah',[TempeeController::class, 'tambah'])->name('tambah');
Route::post('/insertdata',[TempeeController::class, 'insertdata'])->name('insertdata');

Route::get('/tampil/{id}',[TempeeController::class, 'tampil'])->name('tampil');
Route::post('/update/{id}',[TempeeController::class, 'update'])->name('update');

Route::get('/delete/{id}',[TempeeController::class, 'delete'])->name('delete');
