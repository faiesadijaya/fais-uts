<?php

namespace App\Http\Controllers;

use App\models\tempee;
use Illuminate\Http\Request;

class TempeeController extends Controller
{
    public function index(){

        $data = tempee::all();
        return view('datacell',compact('data'));
    }
    public function tambah(){
        return view('tambah');
    }
    public function insertdata(Request $request){
        //dd($request->all());
        tempee::create($request->all());
        return redirect()->route('cell');

    }

    public function tampil($id){
        $data = tempee::find($id);
        //dd($data);
        return view('tampil',compact('data'));
    }
    public function update(request $request,$id){
        $data = tempee::find($id);
        $data->update($request->all());
        return redirect()->route('cell');

    }

    public function delete($id){
        $data = tempee::find($id);
        $data->delete();
        return redirect()->route('cell');
    }
}
